# Tekton Pipelines in OpenShift Example

## Table of Contents
<!-- vim-markdown-toc GitLab -->

* [Introduction](#introduction)
* [Deploying Tekton](#deploying-tekton)
* [Install the Tekton CLI Client (Optional)](#install-the-tekton-cli-client-optional)
* [Create our project](#create-our-project)
* [Create our resources](#create-our-resources)
* [Start the pipeline](#start-the-pipeline)
* [Footnotes](#footnotes)

<!-- vim-markdown-toc -->

## Introduction

In this tutorial we will install Tekton on OpenShift 4.x, with the goal of
building an application once, and then deploying it to two environments, dev
and prod.

## Deploying Tekton

Deploying Tekton (OpenShift Pipelines) on OpenShift 4.x is fairly
straightforward. In the OpenShift web console just click on the "Install"
button. To install from the commandline a single YAML file with a
`Subscription` resource will do:

```bash
$ oc apply -f subscription.yml
```

- ##### **`subscription.yml`**
```yaml
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: openshift-pipelines-operator-rh
  namespace: openshift-operators
spec:
  channel: ocp-4.4
  installPlanApproval: Automatic
  name: openshift-pipelines-operator-rh
  source: redhat-operators
  sourceNamespace: openshift-marketplace
```

## Install the Tekton CLI Client (Optional)

Follow [these instructions](https://github.com/tektoncd/cli#installing-tkn) to
download and install the `tkn` commandline client for your system. This client is not required to work with Tekton, but it makes starting pipelines manually and following/viewing their logs a lot easier.

## Create our project
We will be deploying an app called ["Is It
Friday?"](https://gitlab.com/wanderb/isitfriday). To begin we will create a new
project called `isitfriday-cicd` to house our builds and our pipeline and
related resources. We will also assign both `self-provisioner` and `basic-user`
roles to the `pipeline` serviceaccount, so that it can create new projects by
itself.[^1]

```bash
$ oc new-project isitfriday-cicd
$ oc adm policy add-cluster-role-to-user self-provisioner -z pipeline
$ oc adm policy add-cluster-role-to-user basic-user -z pipeline
```

## Create our resources

Tekton pipelines consist of little Lego blocks tied together, called Tasks.
These Tasks can take parameters, as well as Inputs and Outputs, defined as
PipelineResources. Our example pipeline looks a little something like this.
Notice how some tasks can be executed in parallel.

For example, creating the namespaces/projects can be done while the S2I build
is running.

```mermaid
graph TD;

classDef Resource fill:#ddd,stroke:#000,stroke-width:0.5pt;
classDef Task fill:#8d8,stroke:#000,stroke-width:0.5pt;

CDP([create-dev-project - Task - create-project]):::Task
CPP([create-prod-project - Task - create-project]):::Task
RE(isitfriday-repo - Git Resource):::Resource

BI(isitfriday-build-img - Image Resource):::Resource
DI(isitfriday-dev-img - Image Resource):::Resource
PI(isitfriday-prod-img - Image Resource):::Resource
TD([tag-dev - Task - tag]):::Task
TP([tag-prod - Task - tag]):::Task
AKWID([rollout-dev - Task - apply-kustomize-with-image]):::Task
AKWIP([rollout-prod - Task - apply-kustomize-with-image]):::Task
CTB([s2i-python-3 - ClusterTask]):::Task

CPP --> TP
RE --> CTB 
CTB --> BI 
BI --> TD 
TD --> DI 
CDP --> TD
DI --> AKWID
AKWID --> TP
DI --> TP
TP --> PI 
PI --> AKWIP
```

Removing the resources from the mix makes the graph a bit more readable:

```mermaid
graph TD;

classDef Task fill:#8d8,stroke:#000,stroke-width:0.5pt;

CDP([create-dev-project - Task - create-project]):::Task
CPP([create-prod-project - Task - create-project]):::Task

TD([tag-dev - Task - tag]):::Task
TP([tag-prod - Task - tag]):::Task
AKWID([rollout-dev - Task - apply-kustomize-with-image]):::Task
AKWIP([rollout-prod - Task - apply-kustomize-with-image]):::Task
CTB([s2i-python-3 - ClusterTask]):::Task

CTB --> TD
CDP --> TD --> AKWID
CPP --> TP
AKWID --> TP --> AKWIP

```

- ##### Tasks
  These consist of one or more steps, performing actions like running
  Source-to-Image(S2i) builds, deploying resource manifests, or anything else
  you can do in a container.
  ```yaml
  apiVersion: tekton.dev/v1alpha1
  kind: Task
  metadata:
    name: apply-kustomize
  spec:
    inputs:
      params:
      - default: openshift/overlays/dev
        description: The directory in the source that contains Kustomize overlays
        name: overlay_dir
        type: string
      resources:
      - name: source
        type: git
    steps:
    - name: apply-kustomize
      image: quay.io/openshift/origin-cli:latest
      resources: {}
      workingDir: /workspace/source
      command:
      - /bin/bash
      - -c
      args:
      - |-
        set -e
        echo Applying Kustomize manifests from $(inputs.params.overlay_dir)
        oc apply -k $(inputs.params.overlay_dir)
        echo ---------------------------------------------------------------------
  ```

- ##### ClusterTasks
  The same as regular Tasks, except these are cluster-scoped, making it even
  easier to share and re-use components across an organization.

- ##### PipelineResources
  (Cluster)Tasks have inputs and outputs. This can be Git repositories, Docker
  images, and more. [^2]
  ```yaml
  apiVersion: tekton.dev/v1alpha1
  kind: PipelineResource
  metadata:
    name: isitfriday-repo
  spec:
    type: git
    params:
    - name: url
      value: https://gitlab.com/wanderb/isitfriday.git
    - name: revision
      value: master
  ```

- ##### TaskRuns
  A single run of a Task or ClusterTask. Useful while developing and debugging.
  Note that a (Cluster)Task itself only specifies names for resources used in
  the Task, and names and defaults for parameters. It is when you create a
  TaskRun that actual resources and parameter values get assigned. This allows
  the same (Cluster)Task to be used and re-used across different Pipelines, for
  different applications.

- ##### Pipelines
  A collection of PipelineResources and (Cluster)Tasks. Sprinkled with
  parameters. The ordering of tasks is done implicitly by looking at inputs and
  outputs, and explicitly with `RunAfter` statements.
  ```yaml
  apiVersion: tekton.dev/v1alpha1
  kind: Pipeline
  metadata:
    name: build-and-deploy
  spec:
    params:
    - name: dev-namespace
      description: the namespace where development takes place
      type: string
    - name: prod-namespace
      description: the namespace where production takes place
      type: string

  ...

    resources:
    - name: git-repo
      type: git
    - name: build-image
      type: image

  ...

    tasks:
    - name: create-dev-project
      taskRef:
        kind: Task
        name: create-project
      params:
      - name: namespace
        value: $(params.dev-namespace)

    - name: build-image
      taskRef:
        kind: ClusterTask
        name: s2i-python-3
      params:
      - name: TLSVERIFY
        value: "false"
      resources:
        inputs:
        - name: source
          resource: git-repo
        outputs:
        - name: image
          resource: build-image

    - name: tag-dev
      taskRef:
        kind: Task
        name: tag
      resources:
        inputs:
        - name: from
          resource: build-image
          from: 
          - build-image
        outputs:
        - name: to
          resource: dev-image
      runAfter:
      - create-dev-project

  ...

  ```

- ##### PipelineRuns
  The execution of a pipeline. Just like with a TaskRun it is only here that
  actual PipelineResources and parameter values get assigned. This way the same
  generic Pipeline can be used for multiple applications and projects.
  ```yaml
  apiVersion: tekton.dev/v1alpha1
  kind: PipelineRun
  metadata:
    generateName: build-and-deploy-run-
  spec:
    pipelineRef:
      name: build-and-deploy
    resources:
    - name: git-repo
      resourceRef:
        name: isitfriday-repo

    ...

    params:
    - name: dev-namespace
      value: isitfriday-dev

    ...
  ```

Let's create everything we need:

```bash
$ oc apply -f pipelines/resources
$ oc apply -f pipelines/tasks
$ oc apply -f pipelines/pipeline.yml
```

## Start the pipeline

You can start the pipeline with the `tkn` command, the OpenShift Web Console,
or by using a `PipelineRun` resource. This repository provides a file called
`pipelinerun.yml` which uses a `generateName` to instantiate a new object every
time it is created or applied. This file ties the correct inputs and outputs to
the (Cluster)Tasks in the Pipeline.

You can also use the `tkn pipeline start` command, either interactively, or by specifying all PipelineResources to be used on the command-line.

```bash
$ oc create -f pipeline/pipelinerun.yml
```

Follow the logs in the OpenShift Web Console, or with the `tkn` command:

```bash
$ tkn pipeline logs -f -L build-and-deploy
```

## Footnotes

[^1]: Assigning these permissions requires `cluster-admin` permissions.

[^2]: There are [plans to move away from PipelineResources in the
      future](https://github.com/tektoncd/pipeline/blob/master/docs/migrating-v1alpha1-to-v1beta1.md#pipelineresources-and-catalog-tasks),
      but for now they are supported. You can replace them with Workspaces and Task
      Results.

[modeline]: # ( vim: set ts=2 sts=2 sw=2 spl=en_gb spell et: )
